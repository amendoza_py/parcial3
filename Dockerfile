FROM php:8.2-apache

# Configura la variable de entorno COMPOSER_ALLOW_SUPERUSER
ENV COMPOSER_ALLOW_SUPERUSER 1
# Instala las dependencias necesarias
#RUN apt-get update && apt-get install -y libpq-dev postgresql-client && \
#    docker-php-ext-install zip gd

# Instala las dependencias necesarias para Composer, para leer archivos Excel y para PostgreSQL
RUN apt-get update && \
    apt-get install -y git unzip libzip-dev libpng-dev libpq-dev postgresql-client && \
    docker-php-ext-install zip gd pdo_pgsql && \
    a2enmod rewrite

# Habilita la extensión PDO PostgreSQL
RUN docker-php-ext-configure pdo_pgsql --with-pdo-pgsql=/usr/local/pgsql && docker-php-ext-install pdo pdo_pgsql

# Descarga e instala Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Instala la biblioteca PhpSpreadsheet
RUN composer require phpoffice/phpspreadsheet
# Copia tus archivos de la aplicación
COPY src/ /var/www/html/

# Expone el puerto 80
EXPOSE 80