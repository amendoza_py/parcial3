<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Importar Datos desde Excel</title>
</head>
<body>
    <h2>Importar Datos desde Excel</h2>
    <form action="" method="post" enctype="multipart/form-data">
        <label for="archivo">Selecciona un archivo Excel:</label>
        <input type="file" name="archivo" id="archivo" accept=".xls, .xlsx" required>
        <br>
        <button type="submit">Importar Datos</button>
    </form>
</body>
</html>


<?php

require 'vendor/autoload.php'; // Asegúrate de cargar el autoloader de Composer

use PhpOffice\PhpSpreadsheet\IOFactory;

// Configuración de la conexión a la base de datos
$host = 'postgres';
$port = '5432';
$dbname = 'postgres';
$user = 'postgres';
$password = 'root';

// Conexión a la base de datos
$conn = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");

// Configura el modo de excepciones para PDO
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// Función para procesar el formulario
function procesarFormulario($conn) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Archivo Excel
        $excelFile = $_FILES['archivo']['tmp_name'];

        // Lee el archivo Excel
        $spreadsheet = IOFactory::load($excelFile);
        $sheet = $spreadsheet->getActiveSheet();

        // Itera sobre las filas del archivo Excel y carga los datos en la base de datos
        foreach ($sheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $data = [];
            foreach ($cellIterator as $cell) {
                $data[] = $cell->getValue();
            }

            // Inserta los datos en la base de datos
            $stmt = $conn->prepare("INSERT INTO paises (continente, codigo, pais) VALUES (?, ?, ?)");
            $stmt->execute($data);
        }

        echo "Datos insertados correctamente.";
    }
}

// Procesar el formulario si se envió
procesarFormulario($conn);

?>

