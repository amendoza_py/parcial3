CREATE DATABASE parcial3;
\c parcial3;

CREATE TABLE paises (
    id serial PRIMARY KEY,
    continente varchar(255),
    codigo integer,
    pais   varchar(255)
);